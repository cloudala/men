// Selecting the products database to use.
use('products');

// Inserting a few documents into the products collection
db.getCollection('products').insertMany([
    {
      "nazwa": "Pomidor",
      "cena": 2.99,
      "opis": "Świeże pomidory z lokalnego gospodarstwa",
      "ilość": 50,
      "jednostka_miary": "kg"
    },
    {
      "nazwa": "Makaron pełnoziarnisty",
      "cena": 4.49,
      "opis": "Makaron z pełnego przemiału, opakowanie 500g",
      "ilość": 30,
      "jednostka_miary": "op."
    },
    {
      "nazwa": "Filet z kurczaka",
      "cena": 12.99,
      "opis": "Filet z kurczaka, paczka 1 kg",
      "ilość": 20,
      "jednostka_miary": "kg"
    },
    {
      "nazwa": "Oliwa z oliwek",
      "cena": 19.99,
      "opis": "Extra virgin oliwa z oliwek, butelka 750ml",
      "ilość": 15,
      "jednostka_miary": "szt."
    },
    {
      "nazwa": "Jabłko",
      "cena": 1.49,
      "opis": "Świeże jabłko, odmiana Gala, opakowanie 1 kg",
      "ilość": 40,
      "jednostka_miary": "kg"
    }
]);
