const express = require("express");
const recordRoutes = express.Router();
const dbo = require("../db/conn");
const ObjectId = require("mongodb").ObjectId;

// GET REQUEST => all products
recordRoutes.route("/products").get(function(req, res) {
    let db_connect = dbo.getDb("products");
    let myQuery = { };
    let sortOptions = { };

    // Adding filtering logic
    const filterParams = ['nazwa', 'cena', 'opis', 'ilość', 'jednostka_miary'];
    filterParams.forEach(param => {
        if (req.query[param]) {
            if (param === 'cena') {
                myQuery[param] = parseFloat(req.query[param]);
            } else if (param === 'ilość') {
                myQuery[param] = parseInt(req.query[param]);
            } else {
                myQuery[param] = req.query[param];
            }
        }
    })

    // Adding sorting logic
    if (req.query.sortBy) {
        sortOptions[req.query.sortBy] = (req.query.sortOrder && parseInt(req.query.sortOrder) === -1) ? -1 : 1;
    }

    db_connect.collection('products').find(myQuery).sort(sortOptions).toArray(function(err, result) {
        if (err) throw err;
        res.json(result);
    });
});

// POST REQUEST => add new product
recordRoutes.route('/products').post(function(req, response) {
    let db_connect = dbo.getDb("products");
    let myObj = {
        nazwa: req.body.nazwa,
        cena: req.body.cena,
        opis: req.body.opis,
        ilość: req.body.ilość,
        jednostka_miary: req.body.jednostka_miary
    }

    db_connect.collection('products').findOne({ nazwa: req.body.nazwa }, function(err, existingProduct) {
        if (err) throw err;
        if (existingProduct) {
            response.status(400).json({ Error: 'Product name must be unique'})
        } else {
            db_connect.collection('products').insertOne(myObj, function(err, res) {
                if (err) throw err;
                response.json(res);
            });
        }
    })
});

// PUT REQUEST => update product with given ID
recordRoutes.route('/products/:id').put(function(req, response) {
    let db_connect = dbo.getDb("products");
    let myQuery = { _id: ObjectId(req.params.id) }
    let newValues = { $set: {} };
    const fieldsToUpdate = ['nazwa', 'cena', 'opis', 'ilość', 'jednostka_miary'];
    fieldsToUpdate.forEach(field => {
        if (req.body[field] !== undefined) {
          newValues.$set[field] = req.body[field];
        }
    });

    db_connect.collection('products').updateOne(myQuery, newValues, function(err, res) {
        if (err) throw err;
        console.log('1 document updated successfully');
        response.json(res);
    });
});

// DELETE REQUEST => delete product with given ID
recordRoutes.route('/products/:id').delete(function(req, response) {
    let db_connect = dbo.getDb("products");
    let myQuery = { _id: ObjectId(req.params.id) };

    db_connect.collection('products').deleteOne(myQuery, function(err, res) {
        if (err) throw err;
        if (res.deletedCount === 0) {
            response.status(404).json({ Error: 'Product not found' });
        } else {
            console.log('1 document deleted');
            response.json(res);
        }
    });
});

// GET REQUEST => show products report
recordRoutes.route("/products/report").get(function(req, res) {
    let db_connect = dbo.getDb("products");

    db_connect.collection('products').aggregate([
        {
          $group: {
            _id: '$nazwa',
            ilość: { $sum: '$ilość' },
            wartość: { $sum: { $multiply: ['$ilość', '$cena'] } }
          }
        },
        {
          $project: {
            _id: 0,
            nazwa: '$_id',
            ilość: 1,
            wartość: 1
          }
        },
        {
            $group: {
              _id: null,
              produkty_raport: { $push: { nazwa: '$nazwa', ilość: '$ilość', wartość: '$wartość' } },
              łączna_wartość: { $sum: '$wartość' }
            }
          },
          {
            $project: {
              _id: 0,
              raport_data: new Date(),
              produkty_raport: 1,
              łączna_wartość: 1
            }
          }
    ]).toArray(function(err, result) {
        if (err) throw err;
        res.json(result);
    });
});

module.exports = recordRoutes;